eval `ssh-agent`

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export DOTFILES="$HOME/$(dirname $(readlink ~/.zshrc))"

source "$DOTFILES/zplug/init.zsh"

zplug "zsh-users/zsh-syntax-highlighting"
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-completions"
zplug "softmoth/zsh-vim-mode"
zplug "romkatv/powerlevel10k", as:theme, depth:1
zplug "zplug/zplug", hook-build:"zplug --self-manage"

(zplug check || zplug install) && zplug load

export HISTFILE="$DOTFILES/.zsh_history" HISTSIZE=1000 SAVEHIST=1000
setopt INC_APPEND_HISTORY
path=(~/.local/bin $path)

export NVM_DIR="$DOTFILES/nvm"
source $NVM_DIR/nvm.sh
source $NVM_DIR/bash_completion

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
source "$DOTFILES/p10k.zsh"

[ -e "$DOTFILES/local.zsh" ] && source "$DOTFILES/local.zsh"
